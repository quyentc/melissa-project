/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

var config = {
    map: {
        '*': {
            stickyHeader:            'js/list'
        }
    },
    paths: {
        slick: 'js/slick.min',
    },
    shim: {
        slick: {
            deps: ['jquery']
        }
    }
};
