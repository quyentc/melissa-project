<?php
namespace Smart\InfoPanel\Block\Widget;

use Magento\Framework\View\Element\Template;
use Magento\Widget\Block\BlockInterface;

class InfoPanel extends Template implements BlockInterface {
    protected $_template = "widget/infopanel.phtml";
}