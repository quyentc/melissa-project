<?php
namespace Smart\Subbaner\Block\Widget;

use Magento\Framework\View\Element\Template;
use Magento\Widget\Block\BlockInterface;

class Subbaner extends Template implements BlockInterface {
    protected $_template = "widget/subbaner.phtml";
}
