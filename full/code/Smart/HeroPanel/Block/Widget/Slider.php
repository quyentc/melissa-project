<?php

namespace Smart\HeroPanel\Block\Widget;

use Magento\Framework\View\Element\Template;
use Magento\Widget\Block\BlockInterface;

use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\View\Element\Template\Context;
use Smart\Social\Model\ResourceModel\Social\CollectionFactory;

class Slider extends Template implements BlockInterface
{
    protected $_template = "widget/slider.phtml";

    protected $_collectionFactory;

    /**
     * Index constructor.
     * @param Context $context
     * @param CollectionFactory $collectionFactory
     */
    public function __construct(
        Context $context,
        CollectionFactory $collectionFactory

    )
    {
        $this->_collectionFactory = $collectionFactory;
        parent::__construct($context);
    }

    public function loadBannerSlide()
    {
        $allSlide = $this->_collectionFactory->create()->addFieldToSelect(['thumbnail', 'name', 'url_banner'])
            ->addFieldToFilter('status', 1)
            ->addFieldToFilter('type_id', 2)
            ->setOrder('updated_at', 'DESC')
            ->setPageSize(10)
            ->load();
        return $allSlide;
    }

    public function getMediaUrl()
    {
        try {
            $mediaUrl = $this->_storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA) . 'faq/tmp/icon/';
        } catch (NoSuchEntityException $e) {
        }
        return $mediaUrl;
    }

}