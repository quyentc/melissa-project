define([
    'jquery',
    'Magento_Ui/js/modal/modal',
    'mage/mage',
    'jquery/ui'
], function ($, modal) {
    'use strict';
    $.widget('sm.processPopupNewsletter', {
        /**
         *
         * @private
         */
        _create: function () {
            var self = this, popup_newsletter_options = {
                    type: 'popup',
                    responsive: true,
                    innerScroll: true,
                    title: this.options.popupTitle,
                    buttons: false,
                    modalClass : 'popup-newsletter'
                };

            modal(popup_newsletter_options, this.element);
            
            var d = new Date();
            d.setTime(d.getTime() + (60 * 1000));
            if ($.cookie("popup_newsletter") !== "1") {
                setTimeout(function() {
                    self.element.modal('openModal');
                }, 1000);
                $.cookie("popup_newsletter", "1", {expires: d});
            }
            
            $("#exit-popup").click(function() {
                self.element.modal('closeModal');
            });  
        },
    });
    return $.sm.processPopupNewsletter;
});
