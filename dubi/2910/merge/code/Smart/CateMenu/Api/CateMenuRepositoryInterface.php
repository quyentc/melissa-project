<?php
namespace Smart\CateMenu\Api;

interface CateMenuRepositoryInterface
{
    public function save(\Smart\CateMenu\Api\Data\CateMenuInterface $category);

    public function getById($categoryId);

    public function delete(\Smart\CateMenu\Api\Data\CateMenuInterface $category);

    public function deleteById($categoryId);
}
