<?php

namespace Smart\Social\Ui\Component\Listing\Column;

use Magento\Framework\Option\ArrayInterface;
use Smart\Social\Model\ResourceModel\Type\CollectionFactory;

class Type implements ArrayInterface
{
    // protected $allType;
    /**
     * @var CollectionFactory
     */
    protected $collectionFactory;

    /**
     * Sliders constructor.
     *
     * @param CollectionFactory $collectionFactory
     */
    public function __construct(CollectionFactory $collectionFactory)
    {
        $this->collectionFactory = $collectionFactory;
    }

    /**
     * @return array
     */
    public function toOptionArray()
    {
        $options = [];
        foreach ($this->toArray() as $value => $label) {
            $options[] = [
                'value' => $value,
                'label' => $label
            ];
        }

        return $options;
    }
    /**
     * @return array
     */
    protected function toArray()
    {
        $options = [];

        $rules = $this->collectionFactory->create();
        foreach ($rules as $rule) {
            $options[$rule->getData('type_id')] = $rule->getData('type_name');
        }
        return $options;
    }

}
