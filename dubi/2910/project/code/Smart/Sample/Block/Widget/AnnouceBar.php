<?php


namespace Smart\Sample\Block\Widget;

use Magento\Framework\View\Element\Template;
use Magento\Widget\Block\BlockInterface;

class AnnouceBar extends Template implements BlockInterface {
    protected $_template = "widget/annoucebar.phtml";
}