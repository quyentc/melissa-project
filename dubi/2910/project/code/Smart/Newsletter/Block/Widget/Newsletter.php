<?php
namespace Smart\Newsletter\Block\Widget;

use Magento\Catalog\Block\Product\Context;
use Magento\Framework\View\Element\Template;
use Magento\Widget\Block\BlockInterface;

class Newsletter extends Template implements BlockInterface
{
protected $_template = 'widget/subscribe.phtml';
}