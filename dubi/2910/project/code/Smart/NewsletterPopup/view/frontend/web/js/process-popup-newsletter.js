
define([
    'jquery',
    'Magento_Ui/js/modal/modal',
    'mage/mage',
    'jquery/ui'
], function ($, modal) {
    'use strict';

    $.widget('sm.processPopupNewsletter', {

        /**
         *
         * @private
         */
        _create: function () {
            var self = this, popup_newsletter_options = {
                    type: 'popup',
                    responsive: true,
                    innerScroll: true,
                    title: this.options.popupTitle,
                    buttons: false,
                    modalClass : 'popup-newsletter'
                };

            modal(popup_newsletter_options, this.element);
            
            setTimeout(function() {
                self.element.modal('openModal');
            }, 3000);
            
            $("#exit-popup").click(function() {
                self.element.modal('closeModal');
            });

            // $("#exit-popup").on("click", function() {
            //     setInterval(function() {
            //         self.element.modal('openModal');
            //     }, 30000);
            //   });  
        },
    });
    return $.sm.processPopupNewsletter;

    
});