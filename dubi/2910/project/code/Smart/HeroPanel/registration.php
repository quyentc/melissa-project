<?php
use Magento\Framework\Component\ComponentRegistrar;

ComponentRegistrar::register(
        \Magento\Framework\Component\ComponentRegistrar::MODULE,
        'Smart_HeroPanel',
        __DIR__
);