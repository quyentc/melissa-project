<?php
/**
 * Created by PhpStorm.
 * User: admin111
 * Date: 19/10/2019
 * Time: 17:48
 */

use Magento\Framework\Component\ComponentRegistrar;

ComponentRegistrar::register(
    ComponentRegistrar::MODULE,
    'Smart_ModalWidget',
    __DIR__
);