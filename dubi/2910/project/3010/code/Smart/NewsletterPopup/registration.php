<?php
use Magento\Framework\Component\ComponentRegistrar;
ComponentRegistrar::register(
        ComponentRegistrar::MODULE,
        'Smart_NewsletterPopup',
        __DIR__);