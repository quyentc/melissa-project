<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Smart\CustomAuthentication\Block\Cart;

use Magento\Customer\Model\Context;
use \Magento\Catalog\Api\ProductRepositoryInterface;
/**
 * Shopping cart block
 *
 * @api
 * @since 100.0.2
 */
class Cart extends \Magento\Checkout\Block\Cart\AbstractCart
{
    /**
     * @var $cartHelper
     */
    protected $cartHelper;

    /**
     * @var ProductRepositoryInterface
     */
    protected $productRepository;


    /**
     * @var \Magento\Framework\App\RequestInterface
     */
    protected $_request;

    /**
     * @var \Smart\CustomAuthentication\Block\Account\Dashboard\Recent
     */
    protected $_orders;

    /**
     * @var \Magento\Catalog\Helper\Image
     */
    protected $_imageHelper;


    /**
     * @var \Magento\Checkout\Helper\Cart
     */
    protected $_cartHelper;

    /**
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param \Magento\Customer\Model\Session $customerSession
     * @param \Magento\Checkout\Model\Session $checkoutSession
     * @param \Magento\Checkout\Helper\Cart $cartHelper
     * @param array $data
     * @codeCoverageIgnore
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Customer\Model\Session $customerSession,
        \Magento\Checkout\Model\Session $checkoutSession,
        \Magento\Checkout\Helper\Cart $cartHelper,
        \Magento\Catalog\Api\ProductRepositoryInterfaceFactory $productRepositoryFactory,
        \Magento\Catalog\Helper\Image $imageHelper,
        ProductRepositoryInterface $productRepository,
        \Magento\Framework\App\RequestInterface $request,
        \Smart\CustomAuthentication\Block\Account\Dashboard\Recent $recent,
        array $data = []
    ) {
        $this->_cartHelper = $cartHelper;
        parent::__construct($context, $customerSession, $checkoutSession, $data);
        $this->_isScopePrivate = true;
        $this->_productRepositoryFactory = $productRepositoryFactory;
        $this->_imageHelper = $imageHelper;
        $this->productRepository = $productRepository;
        $this->_request = $context->getRequest();
        $this->_orders = $recent;
    }


    /**
     * @param $productSku
     * @return string
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getAddToCartUrlForProduct($productSku)
    {
        $product = $this->productRepository->get($productSku);
        return $this->_cartHelper->getAddUrl($product);
    }


    /**
     * @return mixed
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public  function getOrdersFromCustomer(){
        $_orders = $this->_orders->getRecentOrders();
        foreach ($_orders as $key => $_order) {
            $array["order".$key] = $this->getAllItems($_order->getRealOrderId());
        }
        return $array;
    }


    /**
     * @param $order_id
     * @return array
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getAllItems($order_id)
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $order = $objectManager->create('Magento\Sales\Model\Order')->loadByIncrementId($order_id);
        $orderItems = $order->getAllItems();
        $itemQty = array();
        foreach ($orderItems as $key => $item) {
            $product_id = $item->getProductId();
            $product = $this->_productRepositoryFactory->create()->getById($product_id);

            if($product->getTypeId() == \Magento\ConfigurableProduct\Model\Product\Type\Configurable::TYPE_CODE){
                $isConfig = 1;
            }else {
                $isConfig = 0;
            }
            $partent_id = $objectManager->create('Magento\ConfigurableProduct\Model\ResourceModel\Product\Type\Configurable')->getParentIdsByChild($product_id);
            $partent_pro_id = $partent_id[0] ?? "";

            $image = $this->_imageHelper->init($product,'image')->setImageFile($product->getImage())->getUrl();
            $itemQty["item".$key]=array('id' => $product_id,
                             'quantity'=>$item->getQtyOrdered(),
                             'description'=>$item->getDescription(),
                             'name'=>$item->getName(),
                             'price'=>$item->getPrice(),
                             'image'=> $image,
                             'sku'=> $item->getSku(),
                             'type' => $item->getProductType(),
                             'is_config' => $isConfig,
                             'has_parent' => $partent_pro_id,
                             'pro_url' => $product->getProductUrl()
            );
        }
        return $itemQty;
    }
}

