<?php
use Magento\Framework\Component\ComponentRegistrar;
ComponentRegistrar::register(
        ComponentRegistrar::MODULE,
        'Smart_Checkout',
        __DIR__);