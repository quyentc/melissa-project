<?php
namespace Smart\NewSorting\Plugin\Model;

class Config
{


    /**
     * Adding new sort option
     *
     * @param \Magento\Catalog\Model\Config $catalogConfig
     * @param [] $result
     * @return []
     */
    public function afterGetAttributeUsedForSortByArray(
        \Magento\Catalog\Model\Config $subject,
        $result
    ) {
        $result['position'] = __("FEATURED");
        return $result;
    }
}
