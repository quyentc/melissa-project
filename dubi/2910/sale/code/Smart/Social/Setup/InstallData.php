<?php

namespace Smart\Social\Setup;

use Magento\Framework\Setup\InstallDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;

class InstallData implements InstallDataInterface
{
    protected $date;

    public function __construct(
        \Magento\Framework\Stdlib\DateTime\DateTime $date
    ) {
        $this->date = $date;
    }

    public function install(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        //sm_social_banner_type
        $dataBannerTypeRows = [
            [
                'type_id' => '1',
                'type_name' => 'Social'
            ],
            [
                'type_id' => '2',
                'type_name' => 'Banner'
            ]
        ];
        foreach ($dataBannerTypeRows as $data) {
            $setup->getConnection()->insert($setup->getTable('sm_social_banner_type'), $data);
        }
        
        
        
    }
}
