<?php
namespace Smart\NewSorting\Plugin\Product\ProductList;

class Toolbar
{
    const LOW_TO_HIGH_SORT_BY = 'low_to_high';
    const HIGH_TO_LOW_SORT_BY = 'high_to_low';
    const NEW_IN_SORT_BY = 'new_in';
    const BEST_SELLING_SORT_BY = 'best_selling';

    /**
     * Around Plugin
     *
     * @param \Magento\Catalog\Block\Product\ProductList\Toolbar $subject
     * @param \Closure $proceed
     * @param \Magento\Framework\Data\Collection $collection
     * @return \Magento\Catalog\Block\Product\ProductList\Toolbar
     */
    public function aroundSetCollection(
        \Magento\Catalog\Block\Product\ProductList\Toolbar $subject,
        \Closure $proceed,
        $collection
    ) {
        $currentOrder = $subject->getCurrentOrder();
        $currentDirection = $subject->getCurrentDirection();
        $result = $proceed($collection);

        switch ($currentOrder) {

            case self::LOW_TO_HIGH_SORT_BY:
            {
                $subject->getCollection()->setOrder('price', 'asc');
                break;
            }
            case self::HIGH_TO_LOW_SORT_BY:
            {
                $subject->getCollection()->setOrder('price', 'desc');
                break;
            }
            case self::NEW_IN_SORT_BY:
            {
                if ($currentDirection == 'desc') {
                    $subject->getCollection()->setOrder('created_at', 'desc');
                } else {
                    $subject->getCollection()->setOrder('created_at', 'asc');
                }
                break;
            }
            case self::BEST_SELLING_SORT_BY:
            {
                $collection->getSelect()->joinLeft(
                    'sales_order_item',
                    'e.entity_id = sales_order_item.product_id',
                    ['qty_ordered' => 'SUM(sales_order_item.qty_ordered)']
                )
                    ->group('e.entity_id')
                    ->order('qty_ordered ' . $currentDirection);
                break;
            }
                default:
        }

        return $result;
    }
}
