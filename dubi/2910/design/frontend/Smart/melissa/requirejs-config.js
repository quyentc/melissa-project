/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

var config = {
    map: {
        '*': {
            myList:            'js/my-list'
        }
    },
    paths: {
        slick: 'js/slick.min',
    },
    shim: {
        slick: {
            deps: ['jquery']
        }
    }
};
