<?php

namespace Smart\QuickView\Block\Product;

use Magento\Catalog\Model\Product;
use Magento\Catalog\Pricing\Price\RegularPrice;
use Magento\Framework\Pricing\Render;

class ListProduct extends \Magento\Catalog\Block\Product\ListProduct
{
    /**
     * Get product price.
     *
     * @param Product $product
     * @return string
     */
    public function getProductRegularPrice(Product $product)
    {
        $priceRender = $this->getPriceRender();

        $price = '';
        if ($priceRender) {
            $price = $priceRender->render(
                RegularPrice::PRICE_CODE,
                $product,
                [
                    'include_container' => true,
                    'display_minimal_price' => true,
                    'zone' => Render::ZONE_ITEM_LIST,
                    'list_category_page' => true
                ]
            );
        }
    }
}
