<?php
namespace Smart\CateMenu\Model\ResourceModel\CateMenu;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    protected $_idFieldName = 'cate_id';
    protected $_eventPrefix = 'sm_custom_category_menu_collection';
    protected $_eventObject = 'sm_custom_category_menu_collection';

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(
            'Smart\CateMenu\Model\CateMenu',
            'Smart\CateMenu\Model\ResourceModel\CateMenu'
        );
    }

}
