<?php
namespace Smart\Social\Setup;

use Magento\Framework\DB\Adapter\AdapterInterface;
use Magento\Framework\DB\Ddl\Table;
use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

class InstallSchema implements InstallSchemaInterface
{
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $installer = $setup;
        $installer->startSetup();

        if (!$installer->tableExists('sm_social_banner_type')) {
            $table = $installer->getConnection()
                ->newTable($installer->getTable('sm_social_banner_type'))
                ->addColumn('type_id', Table::TYPE_INTEGER, null,
                    ['identity' => true,'unsigned' => true, 'nullable' => false, 'primary' => true,], 'Type ID')
                ->addColumn('type_name', Table::TYPE_TEXT,255,
                    ['unsigned' => true, 'nullable' => false,], 'Banner Type Name')              
                ->setComment('Type To Social Link Table');
            $installer->getConnection()->createTable($table);
            $installer->getConnection()->addIndex(
                $installer->getTable('sm_social_banner_type'),
                $setup->getIdxName(
                    $installer->getTable('sm_social_banner_type'),
                    ['type_id','type_name'],
                    AdapterInterface::INDEX_TYPE_FULLTEXT
                ),
                ['type_id','type_name'],
                AdapterInterface::INDEX_TYPE_FULLTEXT
            );
            
        }

        if (!$installer->tableExists('sm_social')) {
            $table = $installer->getConnection()->newTable(
                    $installer->getTable('sm_social')
            )
                ->addColumn(
                    'social_id',
                    Table::TYPE_INTEGER,
                    null,
                    [
                        'identity' => true,
                        'nullable' => false,
                        'primary'  => true,
                        'unsigned' => true,                                                                                                                                                                                                                                                                                                                                                                                                         
                    ],                                                                                                                                                                                          
                    'ID'
                )
                ->addColumn('thumbnail',Table::TYPE_TEXT,'64k',['nullable' => false],'Thumbnail')
                ->addColumn('name',Table::TYPE_TEXT,255,[],'Name')
                ->addColumn('type_id', Table::TYPE_INTEGER, null, ['unsigned' => true,'nullable' => false, 'default' => '1'], 'Banner Type ID')
                ->addColumn('url_banner', Table::TYPE_TEXT, 255, [], 'Banner Url')
                ->addColumn('status',Table::TYPE_BOOLEAN,null,['nullable' => false, 'unsigned' => true],'Status')
                ->addColumn('created_at',Table::TYPE_TIMESTAMP,null,['nullable' => false, 'default' => Table::TIMESTAMP_INIT],'Created At')
                ->addColumn('updated_at',Table::TYPE_TIMESTAMP,null,['nullable' => false, 'default' => Table::TIMESTAMP_INIT],'Updated At')
                ->addIndex($installer->getIdxName('sm_social', ['social_id']), ['social_id'])
                ->addForeignKey(
                    $installer->getFkName(
                        'sm_social',
                        'type_id',
                        'sm_social_banner_type',
                        'type_id'
                        
                    ),
                    'type_id',
                    $installer->getTable('sm_social_banner_type'),
                    'type_id',
                    Table::ACTION_CASCADE
                )
                ->addIndex(
                    $installer->getIdxName(
                        'sm_social',
                        [
                            'type_id'
                            
                        ],
                        AdapterInterface::INDEX_TYPE_UNIQUE
                    ),
                    [
                        'type_id'
                    ],
                    [
                        'type_id' => AdapterInterface::INDEX_TYPE_UNIQUE
                    ]
                )
                ->setComment("Social Table");

            $installer->getConnection()->createTable($table);

        }
        

        $installer->endSetup();


    }
}
