<?php
namespace Smart\CateMenu\Block;

use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\View\Element\Template\Context;
use Magento\Store\Model\StoreManagerInterface;
use Smart\CateMenu\Model\ResourceModel\CateMenu\CollectionFactory;

class Index extends \Magento\Framework\View\Element\Template
{
    protected $_collectionFactory;

    public $_storeManager;

    /**
     * Index constructor.
     * @param Context $context
     * @param CollectionFactory $collectionFactory
     * @param StoreManagerInterface $storeManager

     */
    public function __construct(
        Context $context,
        CollectionFactory $collectionFactory,
        StoreManagerInterface $storeManager

        ) {
        $this->_collectionFactory = $collectionFactory;
        $this->_storeManager = $storeManager;

        parent::__construct($context);
    }
    public function loadAllCategory()
    {
        $allSlide = $this->_collectionFactory->create()->addFieldToSelect(['name','link'])
        ->addFieldToFilter('status', 1)
        ->setOrder('name', 'ASC')
            ->setPageSize(5)
            ->load();
        return $allSlide;
    }

}
