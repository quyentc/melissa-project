<?php
namespace Smart\Social\Block;

use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\View\Element\Template\Context;
use Magento\Store\Model\StoreManagerInterface;
use Smart\Social\Helper\Data;
use Smart\Social\Model\ResourceModel\Social\CollectionFactory;

class Index extends \Magento\Framework\View\Element\Template
{
    protected $_collectionFactory;
    protected $_helperData;
    public $_storeManager;

    /**
     * Index constructor.
     * @param Context $context
     * @param CollectionFactory $collectionFactory
     * @param StoreManagerInterface $storeManager
     * @param Data $helperData
     */
    public function __construct(
        Context $context,
        CollectionFactory $collectionFactory,
        StoreManagerInterface $storeManager,
        Data $helperData
        ) {
        $this->_collectionFactory = $collectionFactory;
        $this->_storeManager = $storeManager;
        $this->_helperData = $helperData;
        parent::__construct($context);
    }
    public function loadAllSlide()
    {
        $allSlide = $this->_collectionFactory->create()->addFieldToSelect(['thumbnail','name'])
        ->addFieldToFilter('status', 1)
            ->setOrder('updated_at', 'DESC')
            ->setPageSize(10)
            ->load();
        return $allSlide;
    }

    public function isEnable()
    {
        return $this->_helperData->getGeneralConfig('enable');
    }
    public function SocialName()
    {
        return $this->_helperData->getStorefrontConfig('social_name');
    }

    public function getMediaUrl()
    {
        try {
            $mediaUrl = $this->_storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA) . 'faq/tmp/icon/';
        } catch (NoSuchEntityException $e) {
        }
        return $mediaUrl;
    }
}
