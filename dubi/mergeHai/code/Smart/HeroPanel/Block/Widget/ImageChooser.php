<?php
namespace Smart\HeroPanel\Block\Widget;
use Magento\Framework\View\Element\Template;
use Magento\Widget\Block\BlockInterface;
class ImageChooser extends Template implements BlockInterface
{
    protected $_template = "widget/imageChooser.phtml";
}