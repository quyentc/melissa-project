<?php
namespace Smart\Panel\Block\Widget;
use Magento\Framework\View\Element\Template;
use Magento\Widget\Block\BlockInterface;
class AnnounceBar extends Template implements BlockInterface
{
    protected $_template = "widget/announceBar.phtml";
}