<?php
namespace Smart\Social\Block\Adminhtml;

use Magento\Backend\Block\Widget\Grid\Container;

class Social extends Container
{
    protected function _construct()
    {
        $this->_controller = 'adminhtml_social';
        $this->_blockGroup = 'Smart_Social';
        $this->_headerText = __('Manage Social');

        parent::_construct();

        if ($this->_isAllowedAction('Smart_Social::save')) {
            $this->buttonList->update('add', 'label', __('Add Slide'));
        } else {
            $this->buttonList->remove('add');
        }
    }

    protected function _isAllowedAction($resourceId)
    {
        return $this->_authorization->isAllowed($resourceId);
    }
}

