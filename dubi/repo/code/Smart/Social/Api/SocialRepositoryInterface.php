<?php
namespace Smart\Social\Api;

interface SocialRepositoryInterface
{
    public function save(\Smart\Social\Api\Data\SocialInterface $slide);

    public function getById($slideId);

    public function delete(\Smart\Social\Api\Data\SocialInterface $slide);

    public function deleteById($slideId);
}
