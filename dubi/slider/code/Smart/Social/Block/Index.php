<?php
namespace Smart\Social\Block;

use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\View\Element\Template\Context;
use Magento\Store\Model\StoreManagerInterface;
use Smart\Social\Model\ResourceModel\Social\CollectionFactory;

class Index extends \Magento\Framework\View\Element\Template
{
    protected $_collectionFactory;
    protected $_imageHelper;
    public $_storeManager;
    public function __construct(Context $context, CollectionFactory $collectionFactory, StoreManagerInterface $storeManager)
    {
        $this->_collectionFactory = $collectionFactory;
        $this->_storeManager = $storeManager;
        parent::__construct($context);
    }
    public function loadAllSlide()
    {
        $allSlide = $this->_collectionFactory->create()->addFieldToSelect(['thumbnail','name'])
        ->addFieldToFilter('status', 1)
            ->setOrder('updated_at', 'DESC')
            ->setPageSize(10)
            ->load();
        return $allSlide;
    }

    public function getMediaUrl()
    {
        try {
            $mediaUrl = $this->_storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA) . 'faq/tmp/icon/';
        } catch (NoSuchEntityException $e) {
        }
        return $mediaUrl;
    }
}
