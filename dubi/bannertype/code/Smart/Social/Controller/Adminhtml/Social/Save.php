<?php

namespace Smart\Social\Controller\Adminhtml\Social;

use Magento\Backend\App\Action;
use Magento\Framework\App\Request\DataPersistorInterface;
use Magento\Framework\Exception\LocalizedException;
use Smart\Social\Model\Social;

class Save extends \Magento\Backend\App\Action
{
    /**
     * @var DataPersistorInterface
     */
    protected $dataPersistor;

    /**
     * @var \Smart\Social\Model\SocialFactory
     */
    private $allnewsFactory;

    /**
     * @var \Smart\Social\Api\SocialRepositoryInterface
     */
    private $allnewsRepository;

    /**
     * @param Action\Context $context
     * @param DataPersistorInterface $dataPersistor
     * @param \Smart\Social\Model\SocialFactory $allnewsFactory
     * @param \Smart\Social\Api\SocialRepositoryInterface $allnewsRepository
     */
    public function __construct(
        Action\Context $context,
        DataPersistorInterface $dataPersistor,
        \Smart\Social\Model\SocialFactory $allnewsFactory = null,
        \Smart\Social\Api\SocialRepositoryInterface $allnewsRepository = null
    ) {
        $this->dataPersistor = $dataPersistor;
        $this->allnewsFactory = $allnewsFactory
            ?: \Magento\Framework\App\ObjectManager::getInstance()->get(\Smart\Social\Model\SocialFactory::class);
        $this->allnewsRepository = $allnewsRepository
            ?: \Magento\Framework\App\ObjectManager::getInstance()->get(\Smart\Social\Api\SocialRepositoryInterface::class);
        parent::__construct($context);
    }

    /**
     * Authorization level
     *
     * @see _isAllowed()
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Smart_Social::save');
    }

    /**
     * Save action
     *
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        $data = $this->getRequest()->getPostValue();
        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        if ($data) {
            if (isset($data['status']) && $data['status'] === 'true') {
                $data['status'] = Social::STATUS_ENABLED;
            }
            if (empty($data['social_id'])) {
                $data['social_id'] = null;
            }

            /** @var \Smart\Social\Model\Social $model */
            $model = $this->allnewsFactory->create();

            $id = $this->getRequest()->getParam('social_id');
            if ($id) {
                try {
                    $model = $this->allnewsRepository->getById($id);
//                    $data = $this->_filterFoodData($data);
                } catch (LocalizedException $e) {
                    $this->messageManager->addErrorMessage(__('This news no longer exists.'));
                    return $resultRedirect->setPath('*/*/');
                }
            }

            $data = $this->_filterFoodData($data);
            $model->setData($data);

            $this->_eventManager->dispatch(
                'social_social_prepare_save',
                ['social' => $model, 'request' => $this->getRequest()]
            );

            try {
                $this->allnewsRepository->save($model);
                $this->messageManager->addSuccessMessage(__('You saved the news.'));
                $this->dataPersistor->clear('social_social');
                if ($this->getRequest()->getParam('back')) {
                    return $resultRedirect->setPath('*/*/edit', ['social_id' => $model->getId(), '_current' => true]);
                }
                return $resultRedirect->setPath('*/*/');
            } catch (LocalizedException $e) {
                $this->messageManager->addExceptionMessage($e->getPrevious() ?: $e);
            } catch (\Exception $e) {
                $this->messageManager->addExceptionMessage($e, __('Something went wrong while saving the news.'));
            }

            $this->dataPersistor->set('social_social', $data);
            return $resultRedirect->setPath('*/*/edit', ['social_id' => $this->getRequest()->getParam('social_id')]);
        }
        return $resultRedirect->setPath('*/*/');
    }

    public function _filterFoodData(array $rawData)
    {
        //Replace icon with fileuploader field name
        $data = $rawData;
        if (isset($data['thumbnail'][0]['name'])) {
            $data['thumbnail'] = $data['thumbnail'][0]['name'];

        } else {
            $data['thumbnail'] = null;
        }
        return $data;
    }

}
