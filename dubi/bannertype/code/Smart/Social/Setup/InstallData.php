<?php

namespace Smart\Social\Setup;

use Magento\Framework\Setup\InstallDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;

class InstallData implements InstallDataInterface
{
    protected $date;

    public function __construct(
        \Magento\Framework\Stdlib\DateTime\DateTime $date
    ) {
        $this->date = $date;
    }

    public function install(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        $dataSlideRows = [
            [
                'thumbnail' => '11a3e7892f54ab3a2959e4.jpg',
                'name' => 'Image 1',
                'type_id' => '1',
                'url_banner' => '#',
                'status' => 1,
                'updated_at' => $this->date->date(),
                'created_at' => $this->date->date()
            ],
            [
                'thumbnail' => 'How-To-Add-Banner-Slider-In-Magento-2-Homepage.jpg',
                'name' => 'Image 2',
                'type_id' => '1',
                'url_banner' => '#',
                'status' => 1,
                'updated_at' => $this->date->date(),
                'created_at' => $this->date->date()
            ],
            [
                'thumbnail' => '115783931601b5c_1.png',
                'name' => 'Image 3',
                'type_id' => '1',
                'url_banner' => '#',
                'status' => 1,
                'updated_at' => $this->date->date(),
                'created_at' => $this->date->date()
            ],
            [
                'thumbnail' => 'homepage-carousel_3.jpg',
                'name' => 'Image 4',
                'type_id' => '1',
                'url_banner' => '#',
                'status' => 0,
                'updated_at' => $this->date->date(),
                'created_at' => $this->date->date()
            ],
            [
                'thumbnail' => 'banner-772x250_1.png',
                'name' => 'Image 5',
                'type_id' => '1',
                'url_banner' => '#',
                'status' => 1,
                'updated_at' => $this->date->date(),
                'created_at' => $this->date->date()
            ],
            [
                'thumbnail' => 'kisspng_1.png',
                'name' => 'Image 6',
                'type_id' => '1',
                'url_banner' => '#',
                'status' => 1,
                'updated_at' => $this->date->date(),
                'created_at' => $this->date->date()
            ],
            [
                'thumbnail' => 'melissa-logo_5.png',
                'name' => 'Image 7',
                'type_id' => '1',
                'url_banner' => '#',
                'status' => 1,
                'updated_at' => $this->date->date(),
                'created_at' => $this->date->date()
            ]
        ];
        foreach ($dataSlideRows as $data) {
            $setup->getConnection()->insert($setup->getTable('sm_social'), $data);
        }
    }
}
