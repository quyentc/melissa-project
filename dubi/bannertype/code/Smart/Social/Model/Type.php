<?php
namespace Smart\Social\Model;


use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\DataObject\IdentityInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Model\AbstractModel;

class Type extends AbstractModel implements IdentityInterface
{
    

    const CACHE_TAG = 'sm_social_banner_type';

    protected $_cacheTag = self::CACHE_TAG;

    protected $_eventPrefix = 'sm_social_banner_type';

    protected function _construct()
    {
        $this->_init('Smart\Social\Model\ResourceModel\Type');
    }

    public function getIdentities()
    {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }

    public function getDefaultValues()
    {
        $values = [];

        return $values;
    }

    public function getAvailableTypes()
    {
        $adapter = $this->getConnection();
        $select = $adapter->select()
            ->from(self::CACHE_TAG, ['type_id' , 'type_name']);
        $data = $adapter->fetchAll($select);

        $options = [];
        foreach ($data as $key => $value) {
            $options[] = [
                'label' => $value['type_name'],
                'value' => $key['type_id'],
            ];
        }

        return $options;
    }

    /**
     * @param \Smart\Social\Model\Social $banner
     *
     * @return array
     */
    public function getTypeNames(\Smart\Social\Model\Social $banner)
    {
        $adapter = $this->getConnection();
        $select = $adapter->select()
            ->from(self::CACHE_TAG, 'type_name')
            ->where('type_id = ?', (int)$banner->getTypeId());

        return $adapter->fetchCol($select);
    }

}
