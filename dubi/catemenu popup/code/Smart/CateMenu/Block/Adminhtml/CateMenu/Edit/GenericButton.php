<?php
namespace Smart\CateMenu\Block\Adminhtml\CateMenu\Edit;

use Magento\Backend\Block\Widget\Context;
use Smart\CateMenu\Api\CateMenuRepositoryInterface;
use Magento\Framework\Exception\NoSuchEntityException;

class GenericButton
{
    protected $context;

    protected $socialRepository;

    public function __construct(
        Context $context,
        CateMenuRepositoryInterface $socialRepository
    ) {
        $this->context = $context;
        $this->socialRepository = $socialRepository;
    }

    public function getNewsId()
    {
        try {
            return $this->socialRepository->getById(
                $this->context->getRequest()->getParam('cate_id')
            )->getId();
        } catch (NoSuchEntityException $e) {
        }
        return null;
    }

    public function getUrl($route = '', $params = [])
    {
        return $this->context->getUrlBuilder()->getUrl($route, $params);
    }
}

