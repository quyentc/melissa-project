<?php
namespace Smart\Sample\Block\Widget;

use Magento\Catalog\Block\Product\Context;
use Magento\Framework\View\Element\Template;
use Magento\Widget\Block\BlockInterface;

class NewsletterPopup extends Template implements BlockInterface
{
protected $_template = 'widget/newsletterpopup.phtml';

public function getFormActionUrl()
{
    return $this->getUrl('smart_sample/subscriber/new', ['_secure' => true]);
}
}