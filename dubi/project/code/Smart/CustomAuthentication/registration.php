<?php
use Magento\Framework\Component\ComponentRegistrar;

ComponentRegistrar::register(
        \Magento\Framework\Component\ComponentRegistrar::MODULE,
        'Smart_CustomAuthentication',
        __DIR__
);