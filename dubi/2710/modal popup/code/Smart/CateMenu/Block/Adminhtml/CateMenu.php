<?php
namespace Smart\CateMenu\Block\Adminhtml;

use Magento\Backend\Block\Widget\Grid\Container;

class CateMenu extends Container
{
    protected function _construct()
    {
        $this->_controller = 'adminhtml_catemenu';
        $this->_blockGroup = 'Smart_CateMenu';
        $this->_headerText = __('Manage Category');

        parent::_construct();

        if ($this->_isAllowedAction('Smart_CateMenu::save')) {
            $this->buttonList->update('add', 'label', __('Add Category'));
        } else {
            $this->buttonList->remove('add');
        }
    }

    protected function _isAllowedAction($resourceId)
    {
        return $this->_authorization->isAllowed($resourceId);
    }
}

