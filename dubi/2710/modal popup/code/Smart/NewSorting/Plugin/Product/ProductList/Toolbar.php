<?php
namespace Smart\NewSorting\Plugin\Product\ProductList;
 
class Toolbar
{
    const FEATURED_SORT_BY = 'featured';
    const LOW_TO_HIGH_SORT_BY = 'low_to_high';
    const HIGH_TO_LOW_SORT_BY = 'high_to_low';
    const NEW_IN_SORT_BY = 'new_in';
    const BEST_SELLING_SORT_BY = 'best_selling';
 
    /**
     * Around Plugin
     *
     * @param \Magento\Catalog\Block\Product\ProductList\Toolbar $subject
     * @param \Closure $proceed
     * @param \Magento\Framework\Data\Collection $collection
     * @return \Magento\Catalog\Block\Product\ProductList\Toolbar
     */
    public function aroundSetCollection(
        \Magento\Catalog\Block\Product\ProductList\Toolbar $subject,
        \Closure $proceed,
        $collection
    ) {
        $currentOrder = $subject->getCurrentOrder();
        $currentDirection = $subject->getCurrentDirection();
        $result = $proceed($collection);
        
        switch($currentOrder){
            case self::FEATURED_SORT_BY:
            {
                if ($currentDirection == 'desc') {
                    $subject->getCollection()->setOrder('position', 'desc');
                } 
                else {
                    $subject->getCollection()->setOrder('position', 'asc');
                }
                break;
            }
            case self::LOW_TO_HIGH_SORT_BY:
            {
                $subject->getCollection()->setOrder('price', 'asc');
                break;
            }
            case self::HIGH_TO_LOW_SORT_BY:
            {
                $subject->getCollection()->setOrder('price', 'desc');
                break;
            }
            case self::NEW_IN_SORT_BY:
            {
                if ($currentDirection == 'desc') {
                    $subject->getCollection()->setOrder('created_at', 'desc');
                } 
                else {
                    $subject->getCollection()->setOrder('created_at', 'asc');
                }
                break;
            }
            default:
        }
        // if ($currentOrder == self::NEWEST_SORT_BY) {
        //     if ($currentDirection == 'desc') {
        //         $subject->getCollection()->setOrder('created_at', 'desc');
        //     } 
        //     else {
        //         $subject->getCollection()->setOrder('created_at', 'asc');
        //     }
        // }
 
        return $result;
    }
}