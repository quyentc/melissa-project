<?php
namespace Smart\NewSorting\Plugin\Model;
 
class Config
{
    const FEATURED_SORT_BY = 'featured';
    const LOW_TO_HIGH_SORT_BY = 'low_to_high';
    const HIGH_TO_LOW_SORT_BY = 'high_to_low';
    const NEW_IN_SORT_BY = 'new_in';
    const BEST_SELLING_SORT_BY = 'best_selling';
 
    /**
     * Adding new sort option
     *
     * @param \Magento\Catalog\Model\Config $catalogConfig
     * @param [] $result
     * @return []
     */
    public function afterGetAttributeUsedForSortByArray(
        \Magento\Catalog\Model\Config $subject, 
        $result
    ) {
        return array_merge(
            $result, 
            [
                self::FEATURED_SORT_BY => __('featured'),
                self::LOW_TO_HIGH_SORT_BY => __('low_to_high'),
                self::HIGH_TO_LOW_SORT_BY => __('high_to_low'),
                self::NEW_IN_SORT_BY => __('new_in'),
                self::BEST_SELLING_SORT_BY => __('best_selling')

            ]
        );
    }
}