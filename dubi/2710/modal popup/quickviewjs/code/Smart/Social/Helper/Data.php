<?php
namespace Smart\Social\Helper;

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Store\Model\ScopeInterface;

class Data extends AbstractHelper
{
    const XML_PATH_SMART_SOCIAL = 'smart/';

    public function getConfigValue($field, $storeCode = null)
    {
        return $this->scopeConfig->getValue($field, ScopeInterface::SCOPE_STORE, $storeCode);
    }

    public function getGeneralConfig($field, $storeCode = null)
    {
        return $this->getConfigValue(self::XML_PATH_SMART_SOCIAL . 'general/' . $field, $storeCode);
    }

    public function getStorefrontConfig($field, $storeCode = null)
    {
        return $this->getConfigValue(self::XML_PATH_SMART_SOCIAL . 'storefront/' . $field, $storeCode);
    }
}
