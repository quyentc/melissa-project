<?php
namespace Smart\CateMenu\Model;

use Smart\CateMenu\Api\Data\CateMenuInterface;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\DataObject\IdentityInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Model\AbstractModel;

class CateMenu extends AbstractModel implements CateMenuInterface, IdentityInterface
{
    const STATUS_ENABLED = 1;
    const STATUS_DISABLED = 0;

    const CACHE_TAG = 'sm_custom_category_menu';

    protected $_cacheTag = self::CACHE_TAG;

    protected $_eventPrefix = 'sm_custom_category_menu';

    protected function _construct()
    {
        $this->_init('Smart\CateMenu\Model\ResourceModel\CateMenu');
    }

    public function getIdentities()
    {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }

    public function getDefaultValues()
    {
        $values = [];

        return $values;
    }

    public function getAvailableStatuses()
    {
        return [self::STATUS_ENABLED => __('Enabled'), self::STATUS_DISABLED => __('Disabled')];
    }

    public function getId()
    {
        return parent::getData(self::CATE_ID);
    }

    public function getName()
    {
        return $this->getData(self::NAME);
    }

    public function getLink()
    {
        return $this->getData(self::LINK);
    }

    public function getStatus()
    {
        return $this->getData(self::STATUS);
    }

    public function getCreatedAt()
    {
        return $this->getData(self::CREATED_AT);
    }

    public function getUpdatedAt()
    {
        return $this->getData(self::UPDATED_AT);
    }

    public function setId($id)
    {
        return $this->setData(self::CATE_ID, $id);
    }

    public function setName($image)
    {
        return $this->setData(self::NAME, $image);
    }

    public function setLink($description)
    {
        return $this->setData(self::LINK, $description);
    }

    public function setStatus($status)
    {
        return $this->setData(self::STATUS, $status);
    }

    public function setCreatedAt($created_at)
    {
        return $this->setData(self::CREATED_AT, $created_at);
    }

    public function setUpdatedAt($updated_at)
    {
        return $this->setData(self::UPDATED_AT, $updated_at);
    }


}
