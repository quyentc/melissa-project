<?php

namespace Smart\CateMenu\Controller\Adminhtml\CateMenu;

use Magento\Backend\App\Action;
use Magento\Framework\App\Request\DataPersistorInterface;
use Magento\Framework\Exception\LocalizedException;
use Smart\CateMenu\Model\CateMenu;

class Save extends \Magento\Backend\App\Action
{
    /**
     * @var DataPersistorInterface
     */
    protected $dataPersistor;

    /**
     * @var \Smart\CateMenu\Model\CateMenuFactory
     */
    private $allnewsFactory;

    /**
     * @var \Smart\CateMenu\Api\CateMenuRepositoryInterface
     */
    private $allnewsRepository;

    /**
     * @param Action\Context $context
     * @param DataPersistorInterface $dataPersistor
     * @param \Smart\CateMenu\Model\CateMenuFactory $allnewsFactory
     * @param \Smart\CateMenu\Api\CateMenuRepositoryInterface $allnewsRepository
     */
    public function __construct(
        Action\Context $context,
        DataPersistorInterface $dataPersistor,
        \Smart\CateMenu\Model\CateMenuFactory $allnewsFactory = null,
        \Smart\CateMenu\Api\CateMenuRepositoryInterface $allnewsRepository = null
    ) {
        $this->dataPersistor = $dataPersistor;
        $this->allnewsFactory = $allnewsFactory
            ?: \Magento\Framework\App\ObjectManager::getInstance()->get(\Smart\CateMenu\Model\CateMenuFactory::class);
        $this->allnewsRepository = $allnewsRepository
            ?: \Magento\Framework\App\ObjectManager::getInstance()->get(\Smart\CateMenu\Api\CateMenuRepositoryInterface::class);
        parent::__construct($context);
    }

    /**
     * Authorization level
     *
     * @see _isAllowed()
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Smart_CateMenu::save');
    }

    /**
     * Save action
     *
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        $data = $this->getRequest()->getPostValue();
        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        if ($data) {
            if (isset($data['status']) && $data['status'] === 'true') {
                $data['status'] = CateMenu::STATUS_ENABLED;
            }
            if (empty($data['cate_id'])) {
                $data['cate_id'] = null;
            }

            /** @var \Smart\CateMenu\Model\CateMenu $model */
            $model = $this->allnewsFactory->create();

            $id = $this->getRequest()->getParam('cate_id');
            if ($id) {
                try {
                    $model = $this->allnewsRepository->getById($id);

                } catch (LocalizedException $e) {
                    $this->messageManager->addErrorMessage(__('This news no longer exists.'));
                    return $resultRedirect->setPath('*/*/');
                }
            }


            $model->setData($data);

            $this->_eventManager->dispatch(
                'catemenu_catemenu_prepare_save',
                ['social' => $model, 'request' => $this->getRequest()]
            );

            try {
                $this->allnewsRepository->save($model);
                $this->messageManager->addSuccessMessage(__('You saved the news.'));
                $this->dataPersistor->clear('catemenu_catemenu');
                if ($this->getRequest()->getParam('back')) {
                    return $resultRedirect->setPath('*/*/edit', ['cate_id' => $model->getId(), '_current' => true]);
                }
                return $resultRedirect->setPath('*/*/');
            } catch (LocalizedException $e) {
                $this->messageManager->addExceptionMessage($e->getPrevious() ?: $e);
            } catch (\Exception $e) {
                $this->messageManager->addExceptionMessage($e, __('Something went wrong while saving the category.'));
            }

            $this->dataPersistor->set('catemenu_catemenu', $data);
            return $resultRedirect->setPath('*/*/edit', ['cate_id' => $this->getRequest()->getParam('cate_id')]);
        }
        return $resultRedirect->setPath('*/*/');
    }


}
