<?php
namespace Smart\Social\Model;

use Smart\Social\Api\Data;
use Smart\Social\Api\SocialRepositoryInterface;
use Magento\Framework\Api\DataObjectHelper;
use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Reflection\DataObjectProcessor;
use Smart\Social\Model\ResourceModel\Social as ResourceAllnews;
use Smart\Social\Model\ResourceModel\Social\CollectionFactory as AllnewsCollectionFactory;
use Magento\Store\Model\StoreManagerInterface;

class SocialRepository implements SocialRepositoryInterface
{
    protected $resource;

    protected $allnewsFactory;

    protected $dataObjectHelper;

    protected $dataObjectProcessor;

    protected $dataAllnewsFactory;

    private $storeManager;

    public function __construct(
        ResourceAllnews $resource,
        SocialFactory $allnewsFactory,
        Data\SocialInterfaceFactory $dataAllnewsFactory,
        DataObjectHelper $dataObjectHelper,
        DataObjectProcessor $dataObjectProcessor,
        StoreManagerInterface $storeManager
    ) {
        $this->resource = $resource;
        $this->allnewsFactory = $allnewsFactory;
        $this->dataObjectHelper = $dataObjectHelper;
        $this->dataAllnewsFactory = $dataAllnewsFactory;
        $this->dataObjectProcessor = $dataObjectProcessor;
        $this->storeManager = $storeManager;
    }

    public function save(\Smart\Social\Api\Data\SocialInterface $slide)
    {
        if ($slide->getStoreId() === null) {
            $storeId = $this->storeManager->getStore()->getId();
            $slide->setStoreId($storeId);
        }
        try {
            $this->resource->save($slide);
        } catch (\Exception $exception) {
            throw new CouldNotSaveException(
                __('Could not save the news: %1', $exception->getMessage()),
                $exception
            );
        }
        return $slide;
    }

    public function getById($slideId)
    {
        $news = $this->allnewsFactory->create();
        $news->load($slideId);
        if (!$news->getId()) {
            throw new NoSuchEntityException(__('News with id "%1" does not exist.', $slideId));
        }
        return $news;
    }

    public function delete(\Smart\Social\Api\Data\SocialInterface $slide)
    {
        try {
            $this->resource->delete($slide);
        } catch (\Exception $exception) {
            throw new CouldNotDeleteException(__(
                'Could not delete the news: %1',
                $exception->getMessage()
            ));
        }
        return true;
    }

    public function deleteById($slideId)
    {
        return $this->delete($this->getById($slideId));
    }
}

