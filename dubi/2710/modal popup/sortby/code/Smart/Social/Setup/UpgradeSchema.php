<?php
namespace Smart\Social\Setup;

use Magento\Framework\DB\Adapter\AdapterInterface;
use Magento\Framework\DB\Ddl\Table;
use Magento\Framework\Setup\ModuleContextInterface;

use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\Setup\UpgradeSchemaInterface;

class UpgradeSchema implements UpgradeSchemaInterface
{
    public function upgrade(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $installer = $setup;

        $installer->startSetup();

        if (version_compare($context->getVersion(), '1.0.2', '<')) {
            if (!$installer->tableExists('sm_social')) {
                $table = $installer->getConnection()->newTable(
                    $installer->getTable('sm_social')
                )
                ->addColumn(
                    'social_id',
                    Table::TYPE_INTEGER,
                    null,
                    [
                        'identity' => true,
                        'nullable' => false,
                        'primary'  => true,
                        'unsigned' => true,
                    ],
                    'ID'
                )
                ->addColumn('thumbnail',Table::TYPE_TEXT,'64k',['nullable' => false],'Thumbnail')
                ->addColumn('name',Table::TYPE_TEXT,255,[],'Name')
                ->addColumn('type_id', Table::TYPE_INTEGER, null, ['unsigned' => true,'nullable' => false, 'default' => '1'], 'Banner Type ID')
                ->addColumn('url_banner', Table::TYPE_TEXT, 255, [], 'Banner Url')
                ->addColumn('status',Table::TYPE_BOOLEAN,null,['nullable' => false, 'unsigned' => true],'Status')
                ->addColumn('created_at',Table::TYPE_TIMESTAMP,null,['nullable' => false, 'default' => Table::TIMESTAMP_INIT],'Created At')
                ->addColumn('updated_at',Table::TYPE_TIMESTAMP,null,['nullable' => false, 'default' => Table::TIMESTAMP_INIT],'Updated At')
                ->setComment("Social Table");

                $installer->getConnection()->createTable($table);

                $installer->getConnection()->addIndex(
                    $installer->getTable('sm_social'),
                    $setup->getIdxName(
                        $installer->getTable('sm_social'),
                        ['thumbnail','name','url_banner'],
                        AdapterInterface::INDEX_TYPE_FULLTEXT
                    ),
                    ['thumbnail','name','url_banner'],
                    AdapterInterface::INDEX_TYPE_FULLTEXT
                );
            }
            if (!$installer->tableExists('sm_social_banner_type')) {
                $table = $installer->getConnection()->newTable(
                    $installer->getTable('sm_social_banner_type')
                )
                ->addColumn('type_id', Table::TYPE_INTEGER, null,
                ['identity' => true,'unsigned' => true, 'nullable' => false, 'primary' => true,], 'Type ID')
                ->addColumn('type_name', Table::TYPE_TEXT,255,
                    ['unsigned' => true, 'nullable' => false,], 'Banner Type Name')
                ->addIndex($installer->getIdxName('sm_social_banner_type', ['type_id']), ['type_id'])
                ->addForeignKey(
                    $installer->getFkName(
                        'sm_social_banner_type',
                        'type_id',
                        'sm_social',
                        'type_id'
                    ),
                    'type_id',
                    $installer->getTable('sm_social'),
                    'type_id',
                    Table::ACTION_CASCADE
                )
                ->setComment("Type To Social Link Table");

                $installer->getConnection()->createTable($table);

                $installer->getConnection()->addIndex(
                    $installer->getTable('sm_social_banner_type'),
                    $setup->getIdxName(
                        $installer->getTable('sm_social_banner_type'),
                        ['type_id','type_name'],
                        AdapterInterface::INDEX_TYPE_FULLTEXT
                    ),
                    ['type_id','type_name'],
                    [
                        'type_id' => AdapterInterface::INDEX_TYPE_FULLTEXT
                    ]
                );
            }
            
        }
    }
}
