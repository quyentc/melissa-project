<?php
namespace Smart\CateMenu\Model;

use Magento\Framework\Api\DataObjectHelper;
use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Reflection\DataObjectProcessor;
use Magento\Store\Model\StoreManagerInterface;
use Smart\CateMenu\Api\CateMenuRepositoryInterface;
use Smart\CateMenu\Api\Data;
use Smart\CateMenu\Model\ResourceModel\CateMenu as ResourceAllnews;

class CateMenuRepository implements CateMenuRepositoryInterface
{
    protected $resource;

    protected $allnewsFactory;

    protected $dataObjectHelper;

    protected $dataObjectProcessor;

    protected $dataAllnewsFactory;

    private $storeManager;

    public function __construct(
        ResourceAllnews $resource,
        CateMenuFactory $allnewsFactory,
        Data\CateMenuInterfaceFactory $dataAllnewsFactory,
        DataObjectHelper $dataObjectHelper,
        DataObjectProcessor $dataObjectProcessor,
        StoreManagerInterface $storeManager
    ) {
        $this->resource = $resource;
        $this->allnewsFactory = $allnewsFactory;
        $this->dataObjectHelper = $dataObjectHelper;
        $this->dataAllnewsFactory = $dataAllnewsFactory;
        $this->dataObjectProcessor = $dataObjectProcessor;
        $this->storeManager = $storeManager;
    }

    public function save(\Smart\CateMenu\Api\Data\CateMenuInterface $category)
    {
        if ($category->getStoreId() === null) {
            $storeId = $this->storeManager->getStore()->getId();
            $category->setStoreId($storeId);
        }
        try {
            $this->resource->save($category);
        } catch (\Exception $exception) {
            throw new CouldNotSaveException(
                __('Could not save the news: %1', $exception->getMessage()),
                $exception
            );
        }
        return $category;
    }

    public function getById($categoryId)
    {
        $news = $this->allnewsFactory->create();
        $news->load($categoryId);
        if (!$news->getId()) {
            throw new NoSuchEntityException(__('Category with id "%1" does not exist.', $categoryId));
        }
        return $news;
    }

    public function delete(\Smart\CateMenu\Api\Data\CateMenuInterface $category)
    {
        try {
            $this->resource->delete($category);
        } catch (\Exception $exception) {
            throw new CouldNotDeleteException(__(
                'Could not delete the news: %1',
                $exception->getMessage()
            ));
        }
        return true;
    }

    public function deleteById($categoryId)
    {
        return $this->delete($this->getById($categoryId));
    }
}
