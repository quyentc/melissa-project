<?php
namespace Smart\Social\Model\ResourceModel\Type;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    protected $_idFieldName = 'type_id';
    protected $_eventPrefix = 'sm_social_banner_type_collection';
    protected $_eventObject = 'sm_social_banner_type_collection';

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(
            'Smart\Social\Model\Type',
            'Smart\Social\Model\ResourceModel\Type'
        );
    }

    

}
