<?php
namespace Smart\HelloWorld\Block;
use Magento\Framework\View\Element\Template;
use Magento\Framework\View\Element\Template\Context;
use Smart\HelloWorld\Helper\Data;
class Test extends \Magento\Framework\View\Element\Template
{
    protected $helperData;
    public function __construct(Context $context,Data $helperData)
    {
        $this->helperData = $helperData;
        parent::__construct($context);
    }

    public function sayHello()
    {
        return __($this->helperData->getGeneralConfig('display_text'));
    }
}