<?php
namespace Smart\AutoSearch\Block;

use Magento\Backend\Block\Template\Context;
use Magento\Catalog\Model\CategoryRepository;
use Magento\Catalog\Model\ResourceModel\Category\CollectionFactory;
use Magento\Framework\View\Element\Template;
use Magento\Store\Model\Store;
use Magento\Store\Model\StoreManagerInterface;

class SearchAutocomplete extends Template
{
    protected $_categoryCollectionFactory;
    protected $_storeManager;
    protected $_store;
    protected $_categoryRepository;

    public function __construct(
        Context $context,
        CollectionFactory $categoryCollectionFactory,
        StoreManagerInterface $storeManager,
        Store $store,
        CategoryRepository $categoryRepository,
        array $data = []
    ) {
        $this->_categoryCollectionFactory 	=	$categoryCollectionFactory;
        $this->_storeManager 	=	$storeManager;
        $this->_store = $store;
        $this->_categoryRepository = $categoryRepository;
        parent::__construct($context, $data);
    }

    public function getCategoryCollection($isActive = true, $level = false, $sortBy = false, $pageSize = false)
    {
        $collection = $this->_categoryCollectionFactory->create();
        $collection->addAttributeToSelect('*');
        if ($isActive) {
            $collection->addIsActiveFilter();
        }
        if ($level) {
            $collection->addLevelFilter($level);
        }
        if ($sortBy) {
            $collection->addOrderField($sortBy);
        }
        if ($pageSize) {
            $collection->setPageSize($pageSize);
        }
        $collection->setOrder('position', 'ASC');
        return $collection;
    }
    public function getAjaxUrl()
    {
        return $this->_storeManager->getStore()->getUrl('autosearch/index/index');
    }
    public function getMediaUrl()
    {
        return $this->_storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA);
    }
    public function getRootCategoryId()
    {
        return $this->_store->getStoreRootCategoryId();
    }
    public function getCategories()
    {
        return $this->_categoryRepository->get(2)->getChildrenCategories();
    }
}
