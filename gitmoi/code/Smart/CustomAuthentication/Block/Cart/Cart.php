<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Smart\CustomAuthentication\Block\Cart;

use Magento\Customer\Model\Context;
use \Magento\Framework\Data\Form\FormKey;
use \Magento\Catalog\Api\ProductRepositoryInterface;
/**
 * Shopping cart block
 *
 * @api
 * @since 100.0.2
 */
class Cart extends \Magento\Checkout\Block\Cart\AbstractCart
{
    protected $cartHelper;
    protected $productRepository;
    protected $formKey;
    protected $_request;

    protected $_orders;

    /**
     * @var \Magento\Catalog\Helper\Image
     */
    protected $_imageHelper;

    /**
     * @var \Magento\Catalog\Api\ProductRepositoryInterfaceFactory
     */
    protected $_productRepositoryFactory;

    /**
     * @var \Magento\Catalog\Model\ResourceModel\Url
     */
    protected $_catalogUrlBuilder;

    /**
     * @var \Magento\Framework\App\Http\Context
     */
    protected $httpContext;

    /**
     * @var \Magento\Checkout\Helper\Cart
     */
    protected $_cartHelper;

    /**
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param \Magento\Customer\Model\Session $customerSession
     * @param \Magento\Checkout\Model\Session $checkoutSession
     * @param \Magento\Catalog\Model\ResourceModel\Url $catalogUrlBuilder
     * @param \Magento\Checkout\Helper\Cart $cartHelper
     * @param \Magento\Framework\App\Http\Context $httpContext
     * @param array $data
     * @codeCoverageIgnore
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Customer\Model\Session $customerSession,
        \Magento\Checkout\Model\Session $checkoutSession,
        \Magento\Catalog\Model\ResourceModel\Url $catalogUrlBuilder,
        \Magento\Checkout\Helper\Cart $cartHelper,
        \Magento\Framework\App\Http\Context $httpContext,
        \Magento\Catalog\Api\ProductRepositoryInterfaceFactory $productRepositoryFactory,
        \Magento\Catalog\Helper\Image $imageHelper,
        FormKey $formKey,
        ProductRepositoryInterface $productRepository,
        \Magento\Framework\App\RequestInterface $request,
        \Smart\CustomAuthentication\Block\Account\Dashboard\Recent $recent,
        array $data = []
    ) {
        $this->_cartHelper = $cartHelper;
        $this->_catalogUrlBuilder = $catalogUrlBuilder;
        parent::__construct($context, $customerSession, $checkoutSession, $data);
        $this->_isScopePrivate = true;
        $this->httpContext = $httpContext;
        $this->_productRepositoryFactory = $productRepositoryFactory;
        $this->_imageHelper = $imageHelper;
        $this->formKey = $formKey;
        $this->productRepository = $productRepository;
        $this->_request = $context->getRequest();
        $this->_orders = $recent;
    }


    public function getAddToCartUrlForProduct($productSku)
    {
        $product = $this->productRepository->get($productSku);
        return $this->_cartHelper->getAddUrl($product);
    }


    public  function getOrdersFromCustomer(){
        $_orders = $this->_orders->getRecentOrders();
        foreach ($_orders as $key => $_order) {
            $array["order".$key] = $this->getAllItems($_order->getRealOrderId());
        }
//        var_dump($array);
        return $array;
    }


    /**
     * @param $order_id
     * @return array
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getAllItems($order_id)
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $order = $objectManager->create('Magento\Sales\Model\Order')->loadByIncrementId($order_id);
        $orderItems = $order->getAllItems();
        $itemQty = array();
        foreach ($orderItems as $key => $item) {
            $productID = $item->getProductId();
            $product = $this->_productRepositoryFactory->create()->getById($productID);
            $image = $this->_imageHelper->init($product,'image')->setImageFile($product->getImage())->getUrl();
            $itemQty["item".$key]=array('id' => $productID,
                             'quantity'=>$item->getQtyOrdered(),
                             'description'=>$item->getDescription(),
                             'name'=>$item->getName(),
                             'price'=>$item->getPrice(),
                             'image'=> $image,
                             'sku'=> $item->getSku(),
                             'type' => $item->getProductType(),
            );
        }
        return $itemQty;
    }

    /**
     * get all order id
     *
     * @return array
     */
    public function getOrderId() {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
        $connection = $resource->getConnection();

        //*****************loading Customer session *****************//
        $customerSession = $objectManager->create('Magento\Customer\Model\Session');

        //******** Checking whether customer is logged in or not ********//
        if ($customerSession->isLoggedIn()) {
            $customer_email = $customerSession->getCustomer()->getEmail();

            // ***********Getting order collection using customer email id ***********//
            $order_collection = $objectManager->create('Magento\Sales\Model\Order')->getCollection()->addAttributeToFilter('customer_email', $customer_email);

            foreach ($order_collection as $order){
                $order_id[] = array($order->getEntityId());
            }
            return $order_id;
        }
    }


    /**
     * Prepare Quote Item Product URLs
     *
     * @codeCoverageIgnore
     * @return void
     */
    protected function _construct()
    {
        parent::_construct();
        $this->prepareItemUrls();
    }

    /**
     * prepare cart items URLs
     *
     * @return void
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     */
    public function prepareItemUrls()
    {
        $products = [];
        /* @var $item \Magento\Quote\Model\Quote\Item */
        foreach ($this->getItems() as $item) {
            $product = $item->getProduct();
            $option = $item->getOptionByCode('product_type');
            if ($option) {
                $product = $option->getProduct();
            }

            if ($item->getStoreId() != $this->_storeManager->getStore()->getId() &&
                !$item->getRedirectUrl() &&
                !$product->isVisibleInSiteVisibility()
            ) {
                $products[$product->getId()] = $item->getStoreId();
            }
        }

        if ($products) {
            $products = $this->_catalogUrlBuilder->getRewriteByProductStore($products);
            foreach ($this->getItems() as $item) {
                $product = $item->getProduct();
                $option = $item->getOptionByCode('product_type');
                if ($option) {
                    $product = $option->getProduct();
                }

                if (isset($products[$product->getId()])) {
                    $object = new \Magento\Framework\DataObject($products[$product->getId()]);
                    $item->getProduct()->setUrlDataObject($object);
                }
            }
        }
    }

    /**
     * @codeCoverageIgnore
     * @return bool
     */
    public function hasError()
    {
        return $this->getQuote()->getHasError();
    }

    /**
     * @codeCoverageIgnore
     * @return int
     */
    public function getItemsSummaryQty() {
        return $this->getQuote()->getItemsSummaryQty();
    }

    /**
     * @codeCoverageIgnore
     * @return bool
     */
    public function isWishlistActive()
    {
        $isActive = $this->_getData('is_wishlist_active');
        if ($isActive === null) {
            $isActive = $this->_scopeConfig->getValue(
                    'wishlist/general/active',
                    \Magento\Store\Model\ScopeInterface::SCOPE_STORE
                ) && $this->httpContext->getValue(
                    Context::CONTEXT_AUTH
                );
            $this->setIsWishlistActive($isActive);
        }
        return $isActive;
    }

    /**
     * @codeCoverageIgnore
     * @return string
     */
    public function getCheckoutUrl()
    {
        return $this->getUrl('checkout', ['_secure' => true]);
    }

    /**
     * @return string
     */
    public function getContinueShoppingUrl()
    {
        $url = $this->getData('continue_shopping_url');
        if ($url === null) {
            $url = $this->_checkoutSession->getContinueShoppingUrl(true);
            if (!$url) {
                $url = $this->_urlBuilder->getUrl();
            }
            $this->setData('continue_shopping_url', $url);
        }
        return $url;
    }

    /**
     * @return bool
     * @codeCoverageIgnore
     * @SuppressWarnings(PHPMD.BooleanGetMethodName)
     */
    public function getIsVirtual()
    {
        return $this->_cartHelper->getIsVirtualQuote();
    }

    /**
     * Return list of available checkout methods
     *
     * @param string $alias Container block alias in layout
     * @return array
     */
    public function getMethods($alias)
    {
        $childName = $this->getLayout()->getChildName($this->getNameInLayout(), $alias);
        if ($childName) {
            return $this->getLayout()->getChildNames($childName);
        }
        return [];
    }

    /**
     * Return HTML of checkout method (link, button etc.)
     *
     * @param string $name Block name in layout
     * @return string
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getMethodHtml($name)
    {
        $block = $this->getLayout()->getBlock($name);
        if (!$block) {
            throw new \Magento\Framework\Exception\LocalizedException(__('Invalid method: %1', $name));
        }
        return $block->toHtml();
    }

    /**
     * Return customer quote items
     *
     * @return array
     */
    public function getItems()
    {
        if ($this->getCustomItems()) {
            return $this->getCustomItems();
        }

        return parent::getItems();
    }

    /**
     * @codeCoverageIgnore
     * @return int
     */
    public function getItemsCount()
    {
        return $this->getQuote()->getItemsCount();
    }

    /**
     * Render pagination HTML
     *
     * @return string
     * @since 100.1.7
     */
    public function getPagerHtml()
    {
        return $this->getChildHtml('pager');
    }
}

