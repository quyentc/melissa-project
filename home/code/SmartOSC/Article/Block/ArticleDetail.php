<?php
namespace SmartOSC\Article\Block;
use SmartOSC\Article\Model\ResourceModel\Article\CollectionFactory;
use Magento\Framework\View\Element\Template;
use Magento\Framework\View\Element\Template\Context;
class ArticleDetail extends \Magento\Framework\View\Element\Template
{
    protected $_collectionFactory;
    public function __construct(Context $context, CollectionFactory $collectionFactory)
    {
        $this->_collectionFactory = $collectionFactory;
        parent::__construct($context);
    }

    public function GetIDArticle($id)
    {
        $articles = $this->_collectionFactory->create();

        $item = $articles->loadDataById($id);
        return $item->getData();

    }
}