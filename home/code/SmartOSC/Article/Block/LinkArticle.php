<?php
namespace SmartOSC\Article\Block;
class LinkArticle extends \Magento\Framework\View\Element\Template
{
    protected $_dataHelper;

    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \SmartOSC\Article\Helper\Data $dataHelper
    ){
        parent::__construct($context);
        $this->_dataHelper = $dataHelper;
    }

    public function getArticleLink()
    {
        $articleLink = $this->_dataHelper->getStorefrontConfig('article_link');

        return $articleLink;
    }

    public function getBaseUrl()
    {
        return $this->_storeManager->getStore()->getBaseUrl();
    }
}
?>
