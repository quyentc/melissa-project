<?php
namespace SmartOSC\Article\Controller\Index;

use Magento\Framework\DataObject;

class Index extends \Magento\Framework\App\Action\Action
{
    protected $_pageFactory;
	protected $_helperData;
    protected $_articleFactory;
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
		\SmartOSC\Article\Helper\Data $helperData,
        \Magento\Framework\View\Result\PageFactory $pageFactory,
         \SmartOSC\Article\Model\ArticleFactory $articleFactory
    ) {
        $this->_pageFactory = $pageFactory;
        $this->_articleFactory = $articleFactory;
		$this->_helperData = $helperData;
        return parent::__construct($context);
    }

    public function execute()
    {

		$isTrue = $this->_helperData->getGeneralConfig('enable');
		echo $this->_helperData->getGeneralConfig('article_link');
        if (!$isTrue) {
            $this->_redirect('/');
        }
        $resultPage = $this->_pageFactory->create();
        $resultPage->getConfig()->getTitle()->set(__('All Articles'));
        $textDisplay = new DataObject(['text' => 'Loaded data completed']);
        $this->_eventManager->dispatch('smartosc_article_load_completed', ['lc' => $textDisplay]);
//        echo $textDisplay->getText();
//        exit;

        return $resultPage;
    }
}
