<?php
namespace Smart\Social\Block;
use Magento\Framework\View\Element\Template;
use Magento\Framework\View\Element\Template\Context;
class Index extends \Magento\Framework\View\Element\Template
{
    protected $_collectionFactory;
    public function __construct(Context $context)
    {
        parent::__construct($context);
    }

    public function sayHello()
	{
		return __('Hello World');
	}
}