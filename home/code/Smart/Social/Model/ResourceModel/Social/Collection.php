<?php
namespace Smart\Social\Model\ResourceModel\Social;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    protected $_idFieldName = 'social_id';
    protected $_eventPrefix = 'sm_social_collection';
    protected $_eventObject = 'sm_social_collection';

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(
            'Smart\Social\Model\Social',
            'Smart\Social\Model\ResourceModel\Social'
        );
    }

}
