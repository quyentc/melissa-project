<?php
namespace Smart\Social\Model\Social;

use Magento\Framework\App\Request\DataPersistorInterface;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Filesystem\Driver\File;
use Magento\Store\Model\StoreManagerInterface;
use Smart\Social\Model\ResourceModel\Social\CollectionFactory;

/**
 * Class DataProvider
 */
class DataProvider extends \Magento\Ui\DataProvider\AbstractDataProvider
{
    /**
     * @var \Smart\Social\Model\ResourceModel\Social\Collection
     */
    protected $collection;

    /**
     * @var DataPersistorInterface
     */
    protected $dataPersistor;

    /**
     * @var array
     */
    protected $loadedData;

    public $_storeManager;

    public $_driveFile;

    /**
     * @param string $name
     * @param string $primaryFieldName
     * @param string $requestFieldName
     * @param CollectionFactory $allnewsCollectionFactory
     * @param DataPersistorInterface $dataPersistor
     * @param StoreManagerInterface $storeManager
     * @param File $driveFile
     * @param array $meta
     * @param array $data
     */
    public function __construct(
        $name,
        $primaryFieldName,
        $requestFieldName,
        CollectionFactory $allnewsCollectionFactory,
        DataPersistorInterface $dataPersistor,
        StoreManagerInterface $storeManager,
        File $driveFile,
        array $meta = [],
        array $data = []
    ) {
        $this->collection = $allnewsCollectionFactory->create();
        $this->dataPersistor = $dataPersistor;
        $this->_storeManager = $storeManager;
        $this->_driveFile = $driveFile;
        parent::__construct($name, $primaryFieldName, $requestFieldName, $meta, $data);
        $this->meta = $this->prepareMeta($this->meta);
    }

    /**
     * Prepares Meta
     *
     * @param array $meta
     * @return array
     */
    public function prepareMeta(array $meta)
    {
        return $meta;
    }

    /**
     * Get data
     *
     * @return array
     */
    public function getData()
    {
        if (isset($this->loadedData)) {
            return $this->loadedData;
        }
        $items = $this->collection->getItems();

        /** @var $news \Smart\Social\Model\Social */
        foreach ($items as $news) {                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             
//            $imagePath = $this->getMediaUrl() . $news->getImage();
//            if ($this->_driveFile->isExists($imagePath)) {
            $this->loadedData[$news->getId()] = $news->getData();
            if ($news->getImage()) {
                $m['thumbnail'][0]['name'] = $news->getImage();
                $m['thumbnail'][0]['url'] = $this->getMediaUrl() . $news->getImage();
                $fullData = $this->loadedData;
                $this->loadedData[$news->getId()] = array_merge($fullData[$news->getId()], $m);
            }
//            }
        }

        $data = $this->dataPersistor->get('social_social');
        if (!empty($data)) {
            $news = $this->collection->getNewEmptyItem();
            $news->setData($data);
            $this->loadedData[$news->getId()] = $news->getData();
            $this->dataPersistor->clear('social_social');
        }

        return $this->loadedData;
    }

    public function getMediaUrl()
    {
        try {
            $mediaUrl = $this->_storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA) . 'faq/tmp/icon/';
        } catch (NoSuchEntityException $e) {
        }
        return $mediaUrl;
    }
}
