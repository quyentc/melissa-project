<?php

namespace Smart\Promo\Block\Widget;

use Magento\Framework\View\Element\Template;
use Magento\Widget\Block\BlockInterface;

class Promo extends Template implements BlockInterface
{
    protected $_template = "widget/promo.phtml";
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context
    ) {
        parent::__construct($context);
    }


}
