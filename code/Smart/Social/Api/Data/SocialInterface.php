<?php
namespace Smart\Social\Api\Data;

interface SocialInterface
{
    const SLIDE_ID = 'social_id';
    const IMAGE  = 'thumbnail';
    const DESCRIPTION = 'name';
    const STATUS = 'status';
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    public function getId();

    public function getImage();

    public function getDescription();

    public function getStatus();

    public function getCreatedAt();

    public function getUpdatedAt();

    public function setId($id);

    public function setImage($image);

    public function setDescription($description);

    public function setStatus($status);

    public function setCreatedAt($created_at);

    public function setUpdatedAt($updated_at);
}

