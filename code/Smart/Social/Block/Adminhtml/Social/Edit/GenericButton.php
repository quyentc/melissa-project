<?php
namespace Smart\Social\Block\Adminhtml\Social\Edit;

use Magento\Backend\Block\Widget\Context;
use Smart\Social\Api\SocialRepositoryInterface;
use Magento\Framework\Exception\NoSuchEntityException;

class GenericButton
{
    protected $context;

    protected $socialRepository;

    public function __construct(
        Context $context,
        SocialRepositoryInterface $socialRepository
    ) {
        $this->context = $context;
        $this->socialRepository = $socialRepository;
    }

    public function getNewsId()
    {
        try {
            return $this->socialRepository->getById(
                $this->context->getRequest()->getParam('social_id')
            )->getId();
        } catch (NoSuchEntityException $e) {
        }
        return null;
    }

    public function getUrl($route = '', $params = [])
    {
        return $this->context->getUrlBuilder()->getUrl($route, $params);
    }
}

